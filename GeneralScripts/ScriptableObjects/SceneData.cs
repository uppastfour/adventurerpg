﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SceneData : MonoBehaviour
{
    public GameObject Target;                                           //target gameobject we clicked on
    public IInteractable TargetInteraction;                             //interactble script for handling the click
    [SerializeField] GameData gameData;
    public GameObject Player;

    private void Awake()
    {
        gameData.currentSceneData = this.gameObject.GetComponent<SceneData>();
        Actor playerActor = Player.GetComponent<Actor>();
        Actor targetActor = Target.GetComponent<Actor>();
        gameData.ChangeCurrentActor(playerActor);
        gameData.ChangeCurrentTarget(targetActor);

    }


}
