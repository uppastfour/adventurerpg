﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName ="New Game Data", menuName = "RPG Tools/Data/New Game Data")]
public class GameData : ScriptableObject
{
    public float InteractableOffset = 3;                                //how far away from an interactable we should stop (so we don't stand right on top of NPCs and other Objects)
    public string[] Layers = { "Interactable", "Enemy", "Ground" };     //our layers for checking hits
    public SceneData[] sceneDataArray;
    public SceneData currentSceneData;
    public Actor currentPlayer;
    public Actor currentTarget;
    public UnityEvent actorListeners;
    public UnityEvent targetListeners;

    public void ChangeSceneData(SceneData sd)
    {
        if (Array.IndexOf(sceneDataArray,sd) != -1)
            currentSceneData = sd;
    }

    public void ChangeCurrentActor(Actor actor)
    {
        currentPlayer = actor;
        actorListeners?.Invoke();
    }

    public void ChangeCurrentTarget(Actor target)
    {
        currentTarget = target;
        targetListeners?.Invoke();
    }



}
