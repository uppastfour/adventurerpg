﻿using UnityEngine.Events;

[System.Serializable]
public class IntIntUnityEvent : UnityEvent<int,int>
{
    
}
