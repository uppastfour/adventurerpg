﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat 
{
    public NameVariable statName;

    [Multiline] [SerializeField] private string description;
    public string Description { get { return description; } }

    [SerializeField] private Sprite icon;
    public Sprite Icon { get { return icon; } }

    [SerializeField] private int baseValue;
    [SerializeField] private int modValue;
    public int Value { get { return (baseValue + modValue); } private set { } }

    public Stat(Stat s)
    {
        this.statName = s.statName;
        this.description = s.description;
        this.icon = s.icon;
        this.baseValue = s.baseValue;
        this.modValue = 0;
    }

    public int InitStat()
    {
        SetModValue(0);
        return Value;
    }

    public void SetBaseValue(int value)
    {
        baseValue = value;
        InitStat();
    }

    public void SetModValue(int v)
    {
        modValue = v;
    }

    public int ChangeBaseValue(int amt)
    {
        baseValue += amt;
        return Value;
    }

    public int ChangeModValue(int amt)
    {
        modValue += amt;
        return Value;
    }
}
