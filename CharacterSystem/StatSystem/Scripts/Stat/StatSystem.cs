﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StatSystem : MonoBehaviour
{
    public Dictionary<NameVariable, Stat> stats = new Dictionary<NameVariable, Stat>();
    public Dictionary<NameVariable, IntUnityEvent> StatEvents = new Dictionary<NameVariable, IntUnityEvent>();
    public Lookup<NameVariable, StatLimiter> limiters;
    public List<PersistentEffect> statEffects = new List<PersistentEffect>();

    [SerializeField] private StatBuilder statTemplate;
    [SerializeField] private StatLimiterBuilder limiterTemplate;

    private void Awake()
    {
        if (statTemplate != null)
        {
            foreach (Stat s in statTemplate.stats)
            {
                Stat newStat = new Stat(s);
                stats[newStat.statName] = newStat;
            }
        }

        if (limiterTemplate != null)
        {
            limiters = (Lookup<NameVariable, StatLimiter>)limiterTemplate.limiters.ToLookup(x => x.StatName);
            
        }
    }

    //add an array of effects from an interaction
    public void AddEffect(Effect[] ef)
    {
        foreach (Effect e in ef)
        {
            //check for instant application
            if (e.OnEnterValue != 0) ApplyEffect(e.item, e.OnEnterValue);
            //check to see if there is a lasting effect
            if (e.PrimaryEffect != null) CheckPersistence(e, e.PrimaryEffect);
        }
    }

    private void ApplyEffect(NameVariable stat, int value)
    {
        print(stats[stat].Value.ToString());
        //apply the value to the mod value of the stat
        int newValue = stats[stat].ChangeModValue(value);
        //make sure we haven't broken a limit rule
        CheckLimiters(stat);
        //contact listeners
        //StatEvents[stat]?.Invoke(newValue);
        print(stats[stat].Value.ToString());
    }

    private void CheckPersistence(Effect effect, Effector effector)
    {
        if (effector.Duration != 0 || effector.PulseRate != 0 || effector.removable == true)
        {
            PersistentEffect pe = new PersistentEffect(effect);
            statEffects.Add(pe);
        }           
    }

    private void UpdateEffects()
    {
        for (int i = statEffects.Count -1; i >=0; i--)
        {
            PersistentEffect stat = statEffects[i];
            if(stat.effect.PrimaryEffect.PulseRate != 0)
            {
                float remainder = stat.appliedTime % stat.effect.PrimaryEffect.PulseRate;
                if(remainder <=0)
                {
                    ApplyEffect(stat.effect.item, stat.effect.PrimaryEffect.Value);
                }
            }

            if(stat.effect.PrimaryEffect.Duration == stat.appliedTime)
            {
                RemoveEffectAt(i);
            }
        }
    }

    private void RemoveEffectAt(int index)
    {
        if (statEffects[index].effect.OnExitValue != 0)
            ApplyEffect(statEffects[index].effect.item, statEffects[index].effect.OnExitValue);
        statEffects.RemoveAt(index);
    }

    public void RemoveEffect(Effect effect)
    { 
        for(int i = statEffects.Count; i >= 0; i--)
        {
            if (statEffects[i].effect == effect)
            {
                RemoveEffectAt(i);
                break;
            }        
        }
    }

    public void ChangeStatBase(NameVariable key, int amt)
    {
        //apply the value to the mod value of the stat
        int newValue = stats[key].ChangeBaseValue(amt);
        //make sure we haven't broken a limit rule
        CheckLimiters(key);
        //contact listeners
        StatEvents[key]?.Invoke(newValue);
    }

    private void CheckLimiters(NameVariable key)
    {
        int currentValue;
        int limiterValue;
        int modValue;
        foreach (var item in limiters[key])
        {
            currentValue = stats[key].Value;
            limiterValue = stats[item.actor].Value;
            modValue = item.CheckValue(currentValue, limiterValue);
            if (modValue != 0) stats[key].ChangeModValue(modValue);
        }
    }
}






