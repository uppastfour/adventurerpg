﻿using UnityEngine;
[CreateAssetMenu(fileName = "New Stat Template", menuName = "RPG Tools/Templates/New Stat Template")]
public class StatBuilder : ScriptableObject
{
    public Stat[] stats;
}