﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Percent Value Limiter",menuName = "RPG Tools/Effects/Limiters/Percent Stat Limiter")]
public class PercentValueLimiter : StatLimiter
{
    [SerializeField] [Range(0, 1)] private float percent;
    public override int CheckValue(int value, int limit)
    {

         int current = (int)(limit * percent);

        if (current != value)
            return current - value;
        else
            return 0;
    }
}
