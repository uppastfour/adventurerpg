﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Equal Value Limiter", menuName = "RPG Tools/Effects/Limiters/Equal Stat Limiter")]
public class EqualValueLimiter : StatLimiter
{
    public override int CheckValue(int value, int limit)
    {
        if (limit == value)
            return 0;
        else 
            return limit - value;
    }
}
