﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Max Value Limiter",menuName = "RPG Tools/Effects/Limiters/Max Stat Value Limiter")]
public class MaxValueLimiter : StatLimiter
{
    public override int CheckValue(int value, int limit)
    {
        if (limit < value)
            return limit - value;
        else
            return 0;
    }
}
