﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatLimiter : ScriptableObject
{
    [SerializeField] private NameVariable statName;
    public NameVariable StatName { get { return statName; } }
    [SerializeField] private NameVariable actorName;
    public NameVariable actor { get { return actorName; } }
    
    public abstract int CheckValue(int value, int limit);
}
