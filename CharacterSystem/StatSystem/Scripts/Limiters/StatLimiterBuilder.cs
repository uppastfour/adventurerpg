﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mod Template", menuName = "RPG Tools/Templates/New Stat Limiter Template")]
public class StatLimiterBuilder : ScriptableObject
{
    public List<StatLimiter>  limiters = new List<StatLimiter>();
}
