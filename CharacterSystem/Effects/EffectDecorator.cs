﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Decorator", menuName = "RPG Tools/Effects/New Effect Decorator")]
public class EffectDecorator : ScriptableObject
{
    public DecoratorType type;
    public NameVariable effectedItem;
    public NameVariable controllingItem;
    [Range(-1, 1)]
    public float percent;
}
public enum DecoratorType
{
    Value,
    Duration,
    Pulse
}
