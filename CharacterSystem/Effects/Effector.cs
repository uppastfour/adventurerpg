﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Effector 
{
    [Tooltip("Value this effector will apply")]
    [SerializeField] private int primaryValue;
    public int Value { get { return primaryValue; } set { primaryValue += value; } }

    [Tooltip("Used in UI to describe what the efftor does")]
    [Multiline] public string description;

    [Tooltip("Any effect that has a duration will be automatically removed at the end of that time")]
    [SerializeField] private int duration;
    public int Duration { get { return duration; } set { duration += value; } }

    [Tooltip("How often does the effect occur? A value of 1 means every second")]
    public int pulse;
    public int PulseRate { get { return pulse; } set { pulse += value; } }

    [Tooltip("Can the effect be removed? If yes it is added to the effect list for the character")]
    public bool removable;
}
