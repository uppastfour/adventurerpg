﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Effect", menuName = "RPG Tools/Effects/New Effect")]
public class Effect : ScriptableObject
{
    [Tooltip("Name of the Effect")]
    public string effectName;
    [Tooltip("What item does this effect")]
    public NameVariable item;
    public Sprite icon;
    [Tooltip("Used in UI to describe what the efftor does")]
    [Multiline] public string description;

    [Tooltip("Value this effector will apply on applied")]
    [SerializeField] private int onEnterValue;
    public int OnEnterValue { get { return onEnterValue; } set { onEnterValue += value; } }
    [Tooltip("What the Primary Purpose of the Effect is")]
    public Effector PrimaryEffect;

    [Tooltip("Value this effector will apply when removed")]
    [SerializeField] private int onExitValue;
    public int OnExitValue { get { return onEnterValue; } set { onEnterValue += value; } }
    public NameVariable[] actorDecorators;
    public NameVariable[] targetDecorators;
}

[System.Serializable]
public class PersistentEffect
{
    public int appliedTime;
    public Effect effect;

    public PersistentEffect(Effect e)
    {
        appliedTime = 0;
        effect = e;
    }
}

