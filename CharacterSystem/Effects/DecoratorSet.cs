﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Decorator Set", menuName = "RPG Tools/Templates/New Decorator Set")]
public class DecoratorSet : ScriptableObject
{
    public List<EffectDecorator> decorators = new List<EffectDecorator>();
}
