﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(StatSystem))]
public class Actor : MonoBehaviour
{
    public StatSystem statSystem;
    public GameData gameData;
    public List<Ability> currentAbilities = new List<Ability>();
    public List<Ability> learnedAbilities = new List<Ability>();
    public Lookup<NameVariable, EffectDecorator> decorators;
    public Lookup<NameVariable, EffectDecorator> modifierDecorators;
    [SerializeField] private DecoratorSet decoratorSet;
    [SerializeField] private DecoratorSet modifierSet;

    void Awake()
    {
        statSystem = GetComponent<StatSystem>();
        decorators = (Lookup<NameVariable, EffectDecorator>)decoratorSet.decorators.ToLookup(x => x.effectedItem);
        modifierDecorators = (Lookup<NameVariable, EffectDecorator>)modifierSet.decorators.ToLookup(x => x.controllingItem);
    }

    public void SetStatEffects(Effect[] effects, Performer type)
    {
        print("Performer: " + type.ToString() + " Effect Count: " + effects.Count().ToString());
        effects = ProcessEffects(effects, type);
        if(type == Performer.Target)
            statSystem.AddEffect(effects);
    }

    /*
    public void SetStatusEffects(Effect[] effects, Performer type)
    {
        effects = ProcessEffects(effects, type);
        statusSystem.AddEffect(effects);
    } 
    */

    private Effect[] ProcessEffects(Effect[] effects, Performer type )
    {
        foreach(Effect effect in effects)
        {
            //get the correct array of decorators to grab
            NameVariable[] decoratorList = type == Performer.Actor ? effect.actorDecorators : effect.targetDecorators;
            float valueDecoration = 0;
            float durationDecoration = 0;

            //grab each decorator based on the list provided
            foreach(NameVariable decoratorName in decoratorList)
            {
                float calculation = 0;
                EffectDecorator current = decorators[effect.item].FirstOrDefault(d => d == decoratorName);
                if(current != default)
                {
                    calculation = statSystem.stats[decoratorName].Value * current.percent;
                    if (current.type == DecoratorType.Value)
                        valueDecoration += calculation;
                    else
                        durationDecoration += calculation;
                }        
            }
            effect.OnEnterValue = (int)valueDecoration;
            effect.PrimaryEffect.Value = (int)valueDecoration;
            effect.OnExitValue = (int)valueDecoration;
            effect.PrimaryEffect.Duration =  (int)valueDecoration;
        }
        return effects;
    }
}

public enum Performer
{
    Actor,
    Target
}
