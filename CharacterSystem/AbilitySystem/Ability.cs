﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="New Ability", menuName = "RPG Tools/Abilities/New Ability")]
public class Ability : ScriptableObject
{
    public string abilityName;
    [Multiline] public string description;
    public Sprite icon;
    public Effect[] statEffects;
    

}
