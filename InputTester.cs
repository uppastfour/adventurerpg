﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTester : MonoBehaviour
{
    public GameData gameData;
    [SerializeField] NameVariable testVariable;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Ability currentAbility = gameData.currentPlayer.currentAbilities[0];
            print("Current Ability: " + currentAbility.abilityName);
            gameData.currentPlayer.SetStatEffects(currentAbility.statEffects,Performer.Actor);
            gameData.currentTarget.SetStatEffects(currentAbility.statEffects, Performer.Target);
        }
            
    }
}
