﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class WorldInteraction : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;                                          //our navmesh agent for movement
    [SerializeField] private Transform playerTransform;                                   //cached player transform
    [SerializeField] private bool hasInteracted = true;                                   //have we interacted with the object yet?
    [SerializeField] private GameData gameData;
     
    // Start is called before the first frame update
    void Start()
    {
        //cache your variables
        agent = GetComponent<NavMeshAgent>();
        playerTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        //check to see if we clicked the mouse and didn't click a UI button
        if(Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            //if so see what we interacted with
            GetInteraction();
        }
        //if we are in the process of interacting handle that
        if (!hasInteracted && agent != null && !agent.pathPending)
        {
            
            //check to see if we are close enough to interact
            if(agent.remainingDistance <= agent.stoppingDistance)
            {
                //if we are call the interact method for the target
                gameData.currentSceneData.TargetInteraction.Interact();

                //make sure we are looking at the target
                EnsureLookDirection();

                //set interacted to true
                hasInteracted = true;
            }
        }
    }

    private void GetInteraction()
    {
        //cast a ray from the camera to where the mouse position was to see what we clicked on
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //if we hit something
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            //get what we hit
            GameObject hitObject = hit.collider.gameObject;
            //if we hit an interactable object
            if (hitObject.layer == LayerMask.NameToLayer("Interactable"))
            {
                //setup the information for the object we clicked
                SetupTarget(hitObject);
                //move to the target so we can interact
                MoveToInteraction();
            }
            //did we click on an enemy?
            else if (hitObject.layer == LayerMask.NameToLayer("Enemy"))
                SetupTarget(hitObject);
            //we clicked on the ground
            else
            {

                //check to see if we are using a ground target skill


                //if not move to the correct location
                agent.stoppingDistance = 0;
                agent.SetDestination(hit.point);
            }
        }
    }

    private void SetupTarget(GameObject go)
    {
        //set target and move towards it
        gameData.currentSceneData.Target = go;

        //cached the interactable script
        gameData.currentSceneData.TargetInteraction = go.GetComponent<IInteractable>();

        //update the UI to display target information
        print(go.layer.ToString());
        
    }

    private void EnsureLookDirection()
    {
        agent.updateRotation = false;
        Vector3 lookDirection = new Vector3(gameData.currentSceneData.Target.transform.position.x, playerTransform.position.y, gameData.currentSceneData.Target.transform.position.z);
        playerTransform.LookAt(lookDirection);
        agent.updateRotation = true;
    }

    private void MoveToInteraction()
    {
        hasInteracted = false;
        agent.stoppingDistance = gameData.InteractableOffset;
        agent.SetDestination(gameData.currentSceneData.Target.transform.position);
    }


}
